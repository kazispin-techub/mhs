@extends('template.layout')

@section('content')
  @guest
   @include('auth.login')
  @endguest
 @auth
 <div class="main-content">
  <div class="container" style="margin-top:10%; width:100%">
    <div class="center" style="padding: 20px"> <h1 class="primary-title">Edit Machine Details</h1></div>
     <div class="row" style="padding:15px">
           <form method="POST"  action="{{ route('machine.update', $machine->id )}}" class="card col s8 offset-s2" style="padding:40px">
              @csrf
              @method('PUT')

              <div class="row">
                  <label for="name">Machine No. : </label>
                      <input type="text" name="machine_no" value="{{ old('name', $machine->machine_no)}}">
                      @error('machine_no')
                          <p>{{ $message }}</p>
                      @enderror
              </div>
              <div class="row">
                  <div>
                    <div class="wrap">
                      <label style="padding-left: 6px">Active</label>
                      <select class="select-css" name="is_active" style="color:#222">
                          <!-- <option value="">Admin:</option> -->
                          <option value="NO"
                              @php
                              echo (old('is_active', $machine->is_active) == 'NO') ? 'selected' : "";
                              @endphp>NO</option>
                          <option value="YES"
                          @php
                          echo (old('is_active', $machine->is_active) == 'YES') ? 'selected' : "";
                          @endphp>YES</option>
                        </select>
                        <?php if (Session::has('error')): ?>
                          <div class="alert alert-danger">


                              {{Session::get('error')}}

                          </div>
                        <?php endif; ?>
                        @error('is_active')
                            <p>{{ $message }}</p>
                        @enderror

                    </div>

                      <!-- <label>Active</label>
                      <select name="is_active">
                        <option value="">Select</option>
                        <option value="NO"
                            @php
                            echo (old('is_active', $machine->is_active) == 'NO') ? 'selected' : "";
                            @endphp>NO</option>
                        <option value="YES"
                        @php
                        echo (old('is_active', $machine->is_active) == 'YES') ? 'selected' : "";
                        @endphp>YES</option>
                      </select>
                      @error('is_active')
                          <p>{{ $message }}</p>
                      @enderror -->
                  </div>
              </div>
              <div class="row">
                  <div class="col s12">
                      <button type="submit" class="btn">Update</button>
                  </div>
              </div>
          </form>
       </div>
<div class="row">
  <div id="picker">
    <input id="demo">
    <button id="toggle">Lanuch</button>
  </div>
</div>

  </div>
 </div>
@endauth
@endsection
@section('customJs')
    <script>
    const picker = new WindowDatePicker({
      el: '#picker',
      toggleEl: '#toggle',
      inputEl: '#demo'
       });
        // document.addEventListener('DOMContentLoaded', function() {
        //     var elems = document.querySelectorAll('select');
        //     var instances = M.FormSelect.init(elems);
        // });
    </script>
@endsection
