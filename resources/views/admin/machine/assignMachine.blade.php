@extends('template.layout')
@section('content')
@guest
@include('auth.login')
@endguest
@auth
<div class="main-content">
  <div class="container" style="margin-top:10%; width:80%">
    <div class="center" style="padding: 20px"> <h1 class="primary-title">Tag Machine User</h1></div>
    <div class="row card" style="padding: 10px">
          <form method="POST"  action="{{ route('assign-machine') }}" class="col s12">
             @csrf

             <div class="row" style="padding-top: 30px">
                 <div class="col s6">
                   <div class="wrap">
                   <label>User Name</label>
                   <select class="select-css" name="user" id="user_id">
                     <option value="" disabled selected>Select</option>
                   @foreach ($users as $user)
                     <option value="{{ $user->id }}">{{ $user->name }}</option>
                   @endforeach
                   </select>
                   @error('name')
                       <p>{{ $message }}</p>
                   @enderror
                   </div>
                 </div>
                 <div class="col s6">
                     <div class="wrap">
                     <label>Machine No.</label>
                     <select class="select-css" name="machine_no" id="machine_id">
                       <option value="" disabled selected>Select</option>
                       @foreach ($machines as $machine)
                         <option value="{{ $machine->id }}">{{ $machine->machine_no }}</option>
                       @endforeach
                     </select>
                     @error('machine_no')
                         <p>{{ $message }}</p>
                     @enderror
                     </div>
                 </div>


                     <!-- <select name="user" id="user_id">

                         <option value="" disabled selected>Select</option>
                       @foreach ($users as $user)
                         <option value="{{ $user->id }}">{{ $user->name }}</option>
                       @endforeach

                     </select>
                     <label>User Name</label>
                     @error('name')
                         <p>{{ $message }}</p>
                     @enderror -->
              </div>

             <div class="row">
                 <div class="input-field col s6">
                     <label for="hourly_session_charge">Hourly session charge : </label>
                     <input type="number" name="hourly_session_charge" value="{{ old('hourly_session_charge')}}" max="999.00">
                     @error('hourly_session_charge')
                         <p>{{ $message }}</p>
                     @enderror
                 </div>
                 <div class="input-field col s6">
                   <div class="wrap">
                    <label>Currency</label>
                   <select class="select-css" name="currency" id="currency">
                     <option value="" disabled selected>Select</option>


                       <option value="BDT">BDT</option>
                       <option value="AED">AED</option>
                       <option value="USD">USD</option>
                   </select>
                   </div>
                   @error('currency')
                       <p>{{ $message }}</p>
                   @enderror
                 </div>
<!--
                     <select name="currency" id="currency">
                       <option value="" disabled selected>Select</option>


                         <option value="BDT">BDT</option>
                         <option value="AED">AED</option>
                         <option value="USD">USD</option>

                     </select>
                     <label>Currency</label> -->
                 </div>
             <div class="row">
                 <div class="col s12">
                     <button type="submit" class="btn">Tag machine</button>
                 </div>
             </div>
         </form>
      </div>
  </div>
</div>
@endauth
@endsection
@section('customJs')
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var elems = document.querySelectorAll('select');
            var instances = M.FormSelect.init(elems);
        });
    </script>
@endsection
