@extends('template.layout')
@section('content')
  @guest
   @include('auth.login')
  @endguest
      @auth
      <div class="main-content">
        <main>
          <div class="cards">
            <a href="{{route('user.create')}}">
              <div class="card-single" style="background:white;color:black;">
                <div>
                  <h4 style="color:black">Add</h4>
                  <span style="color:#424242">Users</span>
                </div>
                <div>
                  <span class="las la-user-plus" style="color:#424242"></span>
                </div >
              </div>
            </a>
            <a href="{{route('machine.create')}}">
            <div class="card-single" style="background:white;color:black;">
              <div>
                <h4 style="color:black">Add</h4>
                <span style="color:#424242">Machine</span>
              </div>
              <div>
                <span class="las la-laptop" style="color:#424242"></span>
              </div>
            </div>
            </a>
            <a href="{{route('user.index')}}">
            <div class="card-single" style="background:white;color:black;">
              <div>
                <h4 style="color:black">User</h4>
                <span style="color:#424242">List</span>
              </div>
              <div>
                <span class="las la-users" style="color:#424242"></span>
              </div>
            </div>
            </a>
            <a href="{{route('machine.index')}}">
            <div class="card-single">
              <div>
                <h4>Machine</h4>
                <span>List</span>
              </div>
              <div>
                <span class="las la-microchip"></span>
              </div>
            </div>
            </a>
            <a href="{{route('assign-machine')}}">
              <div class="card-single" style="background:white;color:black;">
                <div>
                  <h4 style="color:black">Tag</h4>
                  <span style="color:#424242">Machine Users</span>
                </div>
                <div>
                  <span class="las la-tags" style="color:#424242"></span>
                </div >
              </div>
            </a>
            <a href="{{route('detag-machine')}}">
            <div class="card-single"  style="background:white;color:black;">
              <div>
                <h4 style="color:black">Detag</h4>
                <span style="color:#424242">Machine</span>
              </div>
              <div>
                <span class="las la-cut" style="color:#424242"></span>
              </div>
            </div>
            </a>
            <a href="{{route('user-session.index')}}">
            <div class="card-single"  style="background:white;color:black;">
              <div>
                <h4 style="color:black">Create</h4>
                <span style="color:#424242">Sessions</span>
              </div>
              <div>
                <span class="las la-chart-line" style="color:#424242"></span>
              </div>
            </div>
            </a>
            <a href="{{route('edit-rate.index')}}">
            <div class="card-single">
              <div>
                <h4>Edit</h4>
                <span>Session Rate</span>
              </div>
              <div>
                <span class="las la-edit"></span>
              </div>
            </div>
            </a>
            <a href="{{route('invoice.index')}}">
            <div class="card-single"  style="background:white;color:black;">
              <div>
                <h4 style="color:black">Create</h4>
                <span style="color:#424242">Invoice</span>
              </div>
              <div>
                <span class="las la-clipboard" style="color:#424242"></span>
              </div>
            </div>
            </a>
            <a href="{{route('invoice.show',1)}}">
            <div class="card-single"  style="background:white;color:black;">
              <div>
                <h4 style="color:black">Show</h4>
                <span style="color:#424242">Invoice</span>
              </div>
              <div>
                <span class="las la-chart-bar"  style="color:#424242"></span>
              </div>
            </div>
            </a>

          </div>

        </main>

      </div>


            <!-- <div class="col s4">
                <div class="card-panel blue darken-4" style="background: linear-gradient(90deg,#0d47a1 ,#2962ff);">
                    <div class="card-action">
                        <a class="white-text" href="{{route('user.index')}}">Users List</a>
                    </div>
                </div>
            </div>
            <div class="col s4">
                <div class="card-panel indigo darken-4" style="background: linear-gradient(90deg,#1a237e,#5c6bc0);">
                    <div class="card-action">
                        <a class="white-text" href="{{route('user.create')}}">Add user</a>
                    </div>
                </div>
            </div>
            <div class="col s4">
                <div class="card-panel indigo darken-4" style="background: linear-gradient(90deg,#1a237e,#5c6bc0);">
                    <div class="card-action">
                        <a class="white-text" href="{{route('machine.create')}}">Add Machine</a>
                    </div>
                </div>
            </div>
            <div class="col s4">
                <div class="card-panel blue darken-4" style="background: linear-gradient(90deg,#0d47a1 ,#2962ff);">
                    <div class="card-action">
                        <a class="white-text" href="{{route('machine.index')}}">Machine List</a>
                    </div>
                </div>
            </div>
            <div class="col s4">
                <div class="card-panel yellow darken-4">
                    <div class="card-action">
                        <a class="white-text" href="{{route('assign-machine')}}">Tag Machine User</a>
                    </div>
                </div>
            </div>
            <div class="col s4">
                <div class="card-panel red darken-4" style="background: linear-gradient(90deg,#b71c1c,#d50000);">
                    <div class="card-action">
                        <a class="white-text" href="{{route('detag-machine')}}">Detag Machine</a>
                    </div>
                </div>
            </div>
            <div class="col s4">
                <div class="card-panel" style="background: linear-gradient(90deg,#bf360c,#ff5722);">
                    <div class="card-action">
                        <a class="white-text" href="{{route('invoice.index')}}">Create Invoice</a>
                    </div>
                </div>
            </div>
            <div class="col s4">
                <div class="card-panel" style="background: linear-gradient(90deg,#004d40,#26a69a );">
                    <div class="card-action">
                        <a class="white-text" href="{{route('user-session.index')}}">Session Create</a>
                    </div>
                </div>
            </div>
            <div class="col s4">
                <div class="card-panel" style="background: linear-gradient(90deg,#01579b,#039be5);">
                    <div class="card-action">
                        <a class="white-text" href="{{route('edit-rate.index')}}">Edit Session Rate</a>
                    </div>
                </div>
            </div>
            <div class="col s4">
                <div class="card-panel" style="background: linear-gradient(90deg,#33691e,#558b2f );">
                    <div class="card-action">
                        <a class="white-text" href="{{route('invoice.show',1)}}">Show Invoices</a>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
  @endauth
@endsection
