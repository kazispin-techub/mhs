<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Machines extends Model
{
    protected $fillable = [
        'machine_no', 'is_active', 'is_delete', 'is_tagged', 'created_by', 'modified_by'
    ];

    public function taggedUsersMachines(){

        return $this->hasMany(TaggedUsersMachines::class);
    }
}
