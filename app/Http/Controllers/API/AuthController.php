<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class AuthController extends Controller
{
  public function register(Request $request)
  {
//       $data = $request->all();
//       $validator = Validator::make($data, [
//         'first_name' =>'required|string',
//         'last_name' =>'required|string',
//         'name' =>'required|string',
//         'company_name' =>'required|string',
//         'phone_number' =>'required|numeric',
//         'address' =>'required|string',
//         'tax_reg_no' =>'required|numeric',
//         'role_id'=>'required',
//         'bank_account_type'=>'required',
//         'web_site'=>'required',
// //        'email' => 'required|string|email|max:255|unique:users',
//           'email' => 'email|required|unique:users',
//           'password' => 'required|confirmed'
//       ]);
//
//       if($validator->fails()){
//           return response(['error' => $validator->errors(), 'Validation Error']);
//       }
//       else {
//         $user = new User();
//
//         $user->first_name=$request->first_name;
//         $user->last_name=$request->last_name;
//         $user->name=$request->name;
//         $user->email=$request->email;
//         $user->company_name=$request->company_name;
//         $user->phone_number=$request->phone_number;
//         $user->address=$request->address;
//         $user->tax_reg_no=$request->tax_reg_no;
//         $user->bank_account_type=$request->bank_account_type;
//         $user->web_site=$request->web_site;
//         if ($request->role_id == 2) {
//           $user->is_admin="NO";
//         }elseif ($request->role_id == 1) {
//           $user->is_admin="YES";
//         }
//         $user->is_active="YES";
//         $user->created_by = Auth::user()->id;
//         $user->password= Hash::make($request['password']);
//
//
//         $user->save();
//         $user->attachRole($request->role_id);
//         $accessToken = $user->createToken('authToken')->accessToken;
//
//         return response([ 'user' => $user, 'access_token' => $accessToken]);
//
//
//       }


      $validatedData = $request->validate([
        'first_name' =>'required|string',
        'last_name' =>'required|string',
        'name' =>'required|string',
        'company_name' =>'required|string',
        'phone_number' =>'required|numeric',
        'address' =>'required|string',
        'tax_reg_no' =>'required|numeric',
  //      'role_id'=>'required',
        'bank_account_type'=>'required',
        'web_site'=>'required',
//        'email' => 'required|string|email|max:255|unique:users',
          'email' => 'email|required|unique:users',
          'password' => 'required|confirmed'
      ]);

      $validatedData['password'] = bcrypt($request->password);

      $user = User::create($validatedData);

       $accessToken = $user->createToken('authToken')->accessToken;

      return response([ 'user' => $user, 'access_token' => $accessToken]);
  }

    public function login(Request $request)
    {
        $loginData = $request->validate([
            'email' => 'email|required',
            'password' => 'required'
        ]);

        if (!auth()->attempt($loginData)) {
            return response(['message' => 'Invalid Credentials']);
        }

        $accessToken = auth()->user()->createToken('authToken')->accessToken;

        return response(['user' => auth()->user(), 'access_token' => $accessToken]);

    }
}
