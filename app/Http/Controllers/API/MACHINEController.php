<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\MACHINEResource;
use App\Models\Machines;
use Illuminate\Http\Request;

class MACHINEController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $machine= Machines::where('is_delete','NO')->get();
      return response([ 'ceos' => MACHINEResource::collection($machine),
                    'message' => 'Retrieved successfully'], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $data = $request->all();

      $validator = Validator::make($data, [
        'machine_no' => 'required|unique:machines',
      ]);

      if($validator->fails()){
          return response(['error' => $validator->errors(), 'Validation Error']);
      }
      else {
        $machine = new Machines();
        $machine->machine_no = $data->machine_no;
        $machine->is_active = "YES";
        $machine->created_by = Auth::user()->id;
        $machine->save();

      }

//      $ceo = CEO::create($data);

      return response([ 'machine' => new MACHINEResource($machine),
       'message' => 'Created successfully'], 200);


    //  $request->validated();
       //Machines::create($validatedData);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Machines  $machines
     * @return \Illuminate\Http\Response
     */
    public function show(Machines $machines)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Machines  $machines
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Machines $machines)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Machines  $machines
     * @return \Illuminate\Http\Response
     */
    public function destroy(Machines $machines)
    {
        //
    }
}
